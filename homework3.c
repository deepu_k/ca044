#include<stdio.h>
#include<math.h>


void displayCubesSquares(int a)
{
    int ar[a];
    float cu[a];
    float sq[a];
    int i;
    printf("Enter the numbers:\n");

    for(i=0;i<a;i++)
      {
        printf("ar[%d]=",i);
        scanf("%d",&ar[i]);
      }

       printf("The cubes of numbers:\n");
     for(i=0;i<a;i++)
     {
       printf("cu[%d]=%f\n",i,pow(ar[i],3));
     }

       printf("\nThe squares of numbers:\n");
      for(i=0;i<a;i++)
      {
        printf("sq[%d]=%f\n",i,pow(ar[i],2));
      }

}


int main()
{
    int n;
    printf("The number of elements:");
    scanf("%d",&n);
    displayCubesSquares(n);
    return 0;
}
