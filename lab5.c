#include<stdio.h>

void displayMultiple(int n)
{
    int i,m;
    for(i=1;i<=(100/n);i=i+1)
    {
        m=n*i;
        printf("%d * %d = %d\n",n,i,m);
    }
}

int main()
{
    int a;
    printf("Enter a number:");
    scanf("%d",&a);
    displayMultiple(a);
    return 0;

}
