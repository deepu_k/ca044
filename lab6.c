#include<stdio.h>

float average(int ar[50],int n)
{
    int i;
    int sum=0;
    float avg=0.0;

    for(i=0;i<n;i++)
    {
        sum=sum+ar[i];

    }
    avg=(float)sum/n;
    return avg;
}

int main()
{
    int i,n;
    float avg;
    printf("Enter the number of elements:");
    scanf("%d",&n);
    int ar[50];
    for(i=0;i<n;i++)
    {
        printf("ar[%d]=",i);
        scanf("%d",&ar[i]);

    }
    avg=average(ar,n);
    printf("The average of %d numbers is %f\n",n,avg);
    return 0;
}