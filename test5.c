#include<stdio.h>

void interchange(int n)
{
    int i;
    int small=9999;
    int large=-9999;
    int ar[n];
    int temp,pos_large,pos_small;

    printf("Before interchange:\n");

    for(i=0;i<n;i++)
    {
        printf("ar[%d]=",i);
        scanf("%d",&ar[i]);
    }

    for(i=0;i<n;i++)
    {
        if(ar[i]>large)
        {
            large=ar[i];
            pos_large=i;
        }
    }

    for(i=0;i<n;i++)
    {
        if(ar[i]<small)
           {
            small=ar[i];
            pos_small=i;
           }
    }

    temp=ar[pos_large];
    ar[pos_large]=ar[pos_small];
    ar[pos_small]=temp;

    printf("After interchanging:\n");

    for(i=0;i<n;i++)
    {
        printf("ar[%d]=%d\n",i,ar[i]);

    }
}

int main()
{
    int a;
    printf("Enter the number of elements:");
    scanf("%d",&a);
    interchange(a);
    return 0;
}
