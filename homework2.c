#include<stdio.h>

void computeSecondBiggest(int n)
{
    int i,pos;
    int biggest=-9999;
    int secondBiggest=-9998;
    int num[n];
    printf("Enter the numbers:\n");
    for(i=0;i<n;i++)
    {
        printf("num[%d]=",i);
        scanf("%d",&num[i]);
    }
    for(i=0;i<n;i++)
    {
        if(num[i]>biggest)
          {
            biggest=num[i];
          }
    }
     for(i=0;i<n;i++)
     {
         if(num[i]!=biggest)
         {
             if(num[i]>secondBiggest)
             {
                secondBiggest=num[i];
                pos=i;
             }
         }
     }
    printf("The second biggest number is %d\n",secondBiggest);
    printf("The position of second biggest number is %d\n",pos);
}

int main()
{
    int n,s;
    printf("Enter the number of elements:");
    scanf("%d",&n);
    computeSecondBiggest(n);
    return 0;
}
